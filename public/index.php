<?php

use Mpdf\Mpdf;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();
//$app->addRoutingMiddleware();
//$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$app->get('/hello/{name}', function (Request $request, Response $response, $args) {
    $name = $args['name'];  
    $response->getBody()->write("Hello, $name");
    return $response;
});

$app->get('/invoice/{name}', function (Request $request, Response $response, $args) {
    $name = $args['name'];

    $mpdf = new Mpdf();
    $mpdf->WriteHTML('<h1>Hola ' . $name . '!</h1>');

    $response = $response->withHeader('Content-Type', 'application/pdf');
    $response = $response->withHeader('Content-Disposition', 'attachment; filename="invoice.pdf"');

    $pdf = $mpdf->Output('', 'S');
    $response->getBody()->write($pdf);

    return $response;
});